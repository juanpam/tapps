# Readme #

## Description ##
This is the readme of the mobile app TApp, an app developed to display graphically the results of running a given program, written in a SCCP based language implemented as a graduate thesis for my Bachelor on Computer Science at the Pontificia Universidad Javeriana Cali. 

This app is developed using the Ionic 4 framework and is meant to be used as a PWA or as a mobile app for Android devices.
