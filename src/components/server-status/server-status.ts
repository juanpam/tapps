import { Component } from '@angular/core';
import { SccpProvider } from '../../providers/sccp/sccp';
import { Observable } from 'rxjs/Observable';


import 'rxjs/add/observable/timer';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/filter';

/**
 * Generated class for the ServerStatusComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'server-status',
  templateUrl: 'server-status.html'
})
export class ServerStatusComponent {

  text: string;

  status: string;
  statusText: string;
  lastTime: Date;

  isOn = false;

  checking: boolean;

  statusClass = {
    danger: true,
    ok: true
  }

  timeInterval = 3000

  checkObservable: Observable<any>;

  constructor(
    private sccpService: SccpProvider
  ) {
    console.log('Hello ServerStatusComponent Component');
    this.text = 'Hello World';
    this.status = 'OK';

    this.statusClass
    
    this.lastTime = new Date();

    this.checking = true;

    this.checkObservable = Observable.timer(this.timeInterval, this.timeInterval)
    .filter(() => this.isOn)
    .do(() => this.checking = true)
    .switchMap(() => 
      this.sccpService.checkServerStatus()
      .catch(err => Observable.of(err))
      // .do(console.log)
    )
    
    
    this.checkObservable.subscribe(response => {
      this.checking = false;
      this.lastTime = new Date();
      this.status = response.status;
      if(this.status == '200') {
        this.statusClass.ok = true;
        this.statusClass.danger = false;
      } else {
        this.statusClass.ok = false;
        this.statusClass.danger = true;
      }
      this.statusText = response.statusText;
    });
    
  }

}
