import { Component, Input } from '@angular/core';

/**
 * Generated class for the SccpResultsComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sccp-results',
  templateUrl: 'sccp-results.html'
})
export class SccpResultsComponent {

  private _sccpResults:any;

  private sccpResultsStringify: string

  @Input() set sccpResults(sccpResults){
    console.log("sccpResults has changed");
    this.sccpResultsStringify = JSON.stringify(sccpResults);
    this._sccpResults = sccpResults;
  };

  get sccpResults(){ return this._sccpResults }

  constructor() {
  }


}
