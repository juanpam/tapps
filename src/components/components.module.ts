import { NgModule } from '@angular/core';
import { SccpEditorComponent } from './sccp-editor/sccp-editor';
import { SccpResultsComponent } from './sccp-results/sccp-results';
import { ServerStatusComponent } from './server-status/server-status';
@NgModule({
	declarations: [SccpEditorComponent,
    SccpResultsComponent,
    ServerStatusComponent],
	imports: [],
	exports: [SccpEditorComponent,
    SccpResultsComponent,
    ServerStatusComponent]
})
export class ComponentsModule {}
