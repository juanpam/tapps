import { Component, EventEmitter, Output } from '@angular/core';
import { SccpProvider } from '../../providers/sccp/sccp';
import { ToastController } from 'ionic-angular';

import 'rxjs/add/operator/finally';

/**
 * Generated class for the SccpEditorComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sccp-editor',
  templateUrl: 'sccp-editor.html'
})
export class SccpEditorComponent {

  program: string;
  programResults: any;
  depth: number;
  isLoading = false;

  @Output() sccpResults = new EventEmitter<any>();

  constructor(private sccpProvider: SccpProvider, private toastCtrl: ToastController) {
  }

  private runProgram(){
    console.log("Running program ", this.program);
    this.isLoading = true;
    this.sccpProvider.runProgram(this.program, this.depth)
    .finally(() => this.isLoading = false)
    .subscribe(results => {
      this.setProgramResults(results);
      console.log("Program results:", this.programResults);
    }, error => {
      let toast = this.toastCtrl.create({
        message: "There was an error running your program. Please check your internet connection and syntax",
        duration: 5000,
      });
      toast.present();
    });
  }

  private clearProgram(){
    this.program = "";
    this.depth = null;
    this.setProgramResults(undefined);
  }

  private setProgramResults(results){
    this.programResults = results;
    this.sccpResults.emit(this.programResults);
  }
}
