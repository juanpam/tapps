import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  sccpResults: any;

  constructor(public navCtrl: NavController) {

  }

  private onResultsChange(results){
    this.sccpResults = results;
  }

}