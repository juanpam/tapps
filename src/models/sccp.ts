//** This file contains all the models used for the SCCP Programs Interface  */

export class SccpThread {
    spaceId: string;
    k: string;
}

type SccpSpace = string;

export class SccpResults {
    T: {
        threads: SccpThread[],
        spaces: SccpSpace[]
    }
}

export class SccpProgram {
    code: string;
    results: SccpResults;
}