import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { SccpEditorComponent } from '../components/sccp-editor/sccp-editor';
import { SccpResultsComponent } from '../components/sccp-results/sccp-results';
import { SccpProvider } from '../providers/sccp/sccp';
import { HttpClientModule } from '@angular/common/http';
import { ServerStatusComponent } from '../components/server-status/server-status';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    SccpEditorComponent,
    SccpResultsComponent,
    ServerStatusComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    SccpProvider
  ]
})
export class AppModule {}
