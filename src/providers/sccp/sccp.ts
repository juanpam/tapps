import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { SccpResults } from '../../models/sccp';

/*
  Generated class for the SccpProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class SccpProvider {

  private sccpServiceURL = 'https://tesis-juanpam.c9users.io/sccpService';
  private sccpServiceStatusURL = 'https://tesis-juanpam.c9users.io/';

  constructor(public http: HttpClient) {
    console.log('Hello SccpProvider Provider');
  }


  public runProgram(program: string, depth?: number): Observable<SccpResults>{
    let body = {
      sccpProgram: program,
      depth: depth 
    }
    return this.http.post(this.sccpServiceURL, body)
    // .do(response => console.log("Response from server:", response))
    .catch(this.handleError);
  };

  public checkServerStatus(): Observable<HttpResponse<any>>{
    return this.http.get(this.sccpServiceStatusURL, {observe: 'response', responseType: 'text'});
  }


  handleError(error): Observable<any>{
    console.log(error);
    return Observable.throw(error);
  }
}
